This page provides information for categories on Trucks.nl and the webservice that is used to get information about the use of these categories when sending in vehicle information. 

* The webservice to get the current categories, subcategories and makes on Trucks.nl can be found [here](http://www.trucks.nl/StockAPI/OptionsService.wso).
* The Web Service Description Language can be found [here](http://www.trucks.nl/StockAPI/OptionsService.wso?WSDL).

###Vehicletypes

Vehicletypes are the main categories on Trucks.nl. They have a unique Vehicle_type_id that is used to determine what subcategories (constructions) and makes can be used for that vehicle. 

| Paramater | Description |
| --------- | ----------- |
| iLanguageId | Integer that represents a language (See [language](/Languages/) section for more information) | 

This function returns vehicle_type_ids and descriptions of that vehicletype in selected language. 

!!! tip ""
	This function can be called in SOAP 1.1, 1.2 and JSON. For more information click [here](http://www.trucks.nl/StockAPI/OptionsService.wso?op=getVehicleTypes)

###Constructions

Constructions are the subcategories on Trucks.nl. They have a Construction_id which is unique in that range of Vehicletype. 

| Paramater | Description |
| --------- | ----------- |
| iLanguageId | Integer that represents a language (See [language](/Languages/) section for more information) | 
| sVehicleTypeId | Vehicle_type_id |

This function returns construction_ids and descriptions that can be used for selected vehicletype in selected language. 

!!! tip ""
	This function can be called in SOAP 1.1, 1.2 and JSON. For more information click [here](http://www.trucks.nl/StockAPI/OptionsService.wso?op=getConstructions)


###Makes

| Paramater | Description |
| --------- | ----------- |
| sVehicleTypeId | Vehicle_type_id |

This function returns make_ids and descriptions that can be used for selected vehicletype. 

!!! tip "" 
	This function can be called in SOAP 1.1, 1.2 and JSON. For more information click [here](http://www.trucks.nl/StockAPI/OptionsService.wso?op=getMakes)
