### tValue

```
tValue
{
	"key": string,
	"value" : string
}
```

### tVehicleInfo

```
tVehicleInfo
{
	"VehicleId": integer,
	"DealerId" : string,
	{
		tValue, 
		tValue
	},
	{
		string, 
		string
	}
}
```

### tStockInfo

```
tStockInfo
{
	"DealerId": string,
	"max_vehicles": integer, 
	"cur_vehicles": integer,
	"lastupdated": string,
	"type_dealer": string,
	{
		tVehicleInfo,
		tVehicleInfo
	}
}
```

### tPublicDealerInfo

```
tPublicDealerInfo
{
	"DealerId": string,
	"CurVehicles": integer, 
	"MaxVehicles": integer,
	"PicProVehicle": integer,
	"Active": string,	
	"StockOnTrucks": string
}
```

### tResponse

```
tResponse
{
	"code": integer,
	"vehicleid": string, 
	"description": string
}
```

### ValueDescription

```
ValueDescription
{
	"key": string,
	"datatype": string,
	"max_length": string,
	"description": string,
	"possible_values": string,
	"nullable": boolean
}
```

