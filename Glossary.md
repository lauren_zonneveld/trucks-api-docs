### Construction

A construction is a subcategory of a vehicle and is identified with a construction_id. The possible values that can be used for this value is determined by vehicletype of the vehicle.  

!!! tip ""
	More information [here](/Categories/#constructions)

### Dealer 

A dealer is a company that can place stock on Trucks.nl. Each dealer entity is identified with a unique DealerId. Dealers have a contract with Trucks.nl which (also) stated how many vehicles a dealer can advertize on Trucks.nl and how many pictures per vehicle are allowed per agreement. 

!!! important ""
    Information about a Dealer can be accessed by calling the getPublicDealerInfo function.

!!! important ""
    Information about a Dealer and the current stock on Trucks.nl can be accessed by calling the getDealerStock function.

### Make

The value of a make value is determined by vehicletype of the vehicle. 

!!! tip ""
	More information [here](/Categories/#makes)

### Passkey

When authenticating a call to the webservice sometimes a passkey is needed. More information about this can be obtained via email. 

### Reseller

A reseller has a resellerid and a private key that is used in authenticating each call using a passkey

### Vehicle 

A vehicle is any kind of entity that can be placed on Trucks.nl by a dealer. A vehicle has a number of properties, and is uniquely identified witch a VehicleId. All other properties of a vehicle are represented by an array of tuples, with a value field, and a key field. 

!!! important ""
    Information about which values can be used on Trucks.nl per vehicle are provided by the getKeyValues function.


### Vehicletype

A vehicletype is the main category to which a vehicle belongs on Trucks.nl. Each vehicle type also determines which makes and constructions can be used, and is identified with a vehicle_type_id. 

!!! tip ""
	More information [here](/Categories/#vehicletypes)