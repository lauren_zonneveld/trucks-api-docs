Trucks.nl supports a number of languages, these are usualy refered to by a integer number. 

| LanguageId | Language |
| ---------- | -------- |
| 1 | English |
| 2 | Dutch |
| 3 | German |
| 4 | French |
| 5 | Polish |
| 6 | Spanish |
| 11 | Czech | 
| 14 | Russian | 
| 15 | Italian |
| 18 | Portuguese |