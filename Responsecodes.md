If a call to a webservice returns a response struct, it uses an error code. 

| Code | Description | 
| ---- | ----------- | 
| 200 | Vehicle succesfully added | 
| 201 | Vehicle succesfully updated | 
| 202 | Vehicle succesfully deleted | 
| 500 | Maximum allowed vehicles reached |
| 501 | VehicleId not found | 
| 502 | DealerId not found | 
| 503 | DealerId not active | 
| 504 | VehicleTypeId not found | 
| 510 | API use not allowed for this dealer | 
| 601 | Warning: MakeId not found |
| 602 | Warning: ConstructionId not found |  

!!! danger "HTTP response codes"
	All functions in this API will respond with a http 200 status code is called correctly, with the exception of __getPublicDealerInfo__ , this wil return a http 500 response header when dealer is not found/api use is not allowed. 