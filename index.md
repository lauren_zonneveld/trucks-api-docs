This documentation describes the Trucks.nl API SOAP webservice. An overview of the functions used can be found [here](http://www.trucks.nl/StockAPI/TrucksStock.wso).
The service descriptor ([WSDL](http://www.trucks.nl/StockAPI/TrucksStock.wso?WSDL)) can be found [here](http://www.trucks.nl/StockAPI/TrucksStock.wso?WSDL).


##Functions 

| Function name 		  | Description 																	| Returns |
| ----------------------- | -------------------------------------------------------------------------------	| ------- |
| [addVehicle()](#addvehicle)			  | Adds a vehicle to the dealers stock 							| [tResponse](/Datatypes/#tresponse) |
| [updateVehicle()](#updatevehicle)		  | Updates an existing vehicle in the dealers stock 				| [tResponse](/Datatypes/#tresponse) |
| [deleteVehicle()](#deletevehicle)		  | Deletes an existing vehicle from the dealers stock 				| [tResponse](/Datatypes/#tresponse) |
| [getPublicDealerInfo()](#getpublicdealerinfo)	  | Returns information from the dealers 					| [tPublicDealerInfo](/Datatypes/#tpublicdealerinfo) | 
| [getDealerStock()](#getdealerstock)		  | Returns current stock of dealer using vehicleids 			| [tStockInfo](/Datatypes/#tstockinfo) |
| [getKeyDescriptions()](#getkeydescriptions)	  | Returns information about all values that can by used to describe a vehicle 	| array of [ValueDescription](/Datatypes/#valuedescription) | 


###addVehicle

| Parameter | Description |
| --------- | ----------- | 
| DealerId  | Unique dealer identifier |
| ResellerId | Unique reseller identifier | 
| Passkey   | Hashed key |
| Values 	| Array of [tValue](/Datatypes/#tvalue) |
| Pictures	| Array of picture urls | 


__Example SOAP 1.1 request__ 

Add a crawler crane to the stock of dealer with dealerid TESTDEALER

```
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:truc="http://www.trucks.nl/">
  <SOAP-ENV:Body>
    <truc:addVehicle>
      <truc:DealerId>TESTDEALER</truc:DealerId>
      <truc:Passkey>123456768692ea9ac90ba9a3de69eed2</truc:Passkey>
      <truc:ResellerId>TESTRESELLER</truc:ResellerId>
      <truc:Values>
        <truc:tValue>
          <truc:key>VEHICLE_TYPE_ID</truc:key>
          <truc:value>LIFT</truc:value>
        </truc:tValue>
        <truc:tValue>
          <truc:key>DLR_VEHICLE_ID</truc:key>
          <truc:value>TEST</truc:value>
        </truc:tValue>
        <truc:tValue>
          <truc:key>CONSTRUCTION_ID</truc:key>
          <truc:value>CRAWLERLIFT</truc:value>
        </truc:tValue>
        <truc:tValue>
          <truc:key>MAKE_ID</truc:key>
          <truc:value>LINDE</truc:value>
        </truc:tValue>
        <truc:tValue>
          <truc:key>TYPE</truc:key>
          <truc:value>This is a testvehicle</truc:value>
        </truc:tValue>
        <truc:tValue>
          <truc:key>YEAR</truc:key>
          <truc:value>1999</truc:value>
      	</truc:tValue>
      </truc:Values>
      <truc:Pictures>
        <truc:string>http://www.trucks.nl/truckpics/TEST_1.jpg</truc:string>
        <truc:string>http://www.trucks.nl/truckpics/TEST_2.jpg</truc:string>
        <truc:string>http://www.trucks.nl/truckpics/TEST_3.jpg</truc:string>
        <truc:string>http://www.trucks.nl/truckpics/TEST_4.jpg</truc:string>        
      </truc:Pictures>
    </truc:addVehicle>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

__Example SOAP 1.1 response__
```
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <m:addVehicleResponse xmlns:m="http://www.trucks.nl/">
         <m:addVehicleResult>
            <m:code>200</m:code>
            <m:vehicleid>123456</m:vehicleid>
            <m:description>http://www.trucks.nl/123456-vd</m:description>
         </m:addVehicleResult>
      </m:addVehicleResponse>
   </soap:Body>
</soap:Envelope>
```

!!! tip ""
    The service also supports SOAP 1.2 and JSON calls for more information click [here](http://www.trucks.nl/StockAPI/TrucksStock.wso?op=addVehicle)

###updateVehicle

| Parameter | Description |
| --------- | ----------- | 
| VehicleId  | Unique vehicle identifier |
| DealerId  | Unique dealer identifier |
| ResellerId | Unique reseller identifier | 
| Passkey   | Hashed key |
| Values 	| Array of [tValue](/Datatypes/#tvalue) |
| Pictures	| Array of picture urls | 


__Example SOAP 1.1 request__ 

Updates the vehicle created in the addvehicle request, and stes the year to 2000. 

```
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:truc="http://www.trucks.nl/">
  <SOAP-ENV:Body>
    <truc:updateVehicle>
    	<truc:VehicleId>123456</truc:VehicleId>
      	<truc:DealerId>TESTDEALER</truc:DealerId>
      	<truc:Passkey>123456768692ea9ac90ba9a3de69eed2</truc:Passkey>
     	<truc:ResellerId>TESTRESELLER</truc:ResellerId>
      	<truc:Values>       
        	<truc:tValue>
          		<truc:key>YEAR</truc:key>
          		<truc:value>2000</truc:value>
      		</truc:tValue>
  		</truc:Values>     
	</truc:updateVehicle>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

__Example SOAP 1.1 response__
```	
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <m:updateVehicleResponse xmlns:m="http://www.trucks.nl/">
         <m:updateVehicleResult>
            <m:code>201</m:code>
            <m:vehicleid>123456</m:vehicleid>
            <m:description>http://www.trucks.nl/123456-vd</m:description>
         </m:updateVehicleResult>
      </m:updateVehicleResponse>
   </soap:Body>
</soap:Envelope>
```

!!! tip ""
    The service also supports SOAP 1.2 and JSON calls for more information click [here](http://www.trucks.nl/StockAPI/TrucksStock.wso?op=updateVehicle)

!!! danger "Update/change pictures"
	If an array of picture url's is included in a request all other pictures will be changed. So when updating 1 or more picture add all pictures for this vehicle. 

###deleteVehicle

| Parameter | Description |
| --------- | ----------- | 
| VehicleId  | Unique vehicle identifier |
| DealerId  | Unique dealer identifier |
| ResellerId | Unique reseller identifier | 
| Passkey   | Hashed key |


__Example SOAP 1.1 request__ 

Deletes vehicle created by addvehicle request. 

```
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:truc="http://www.trucks.nl/">
  <SOAP-ENV:Body>
    <truc:deleteVehicle>
    	<truc:VehicleId>123456</truc:VehicleId>
      	<truc:DealerId>TESTDEALER</truc:DealerId>
      	<truc:Passkey>123456768692ea9ac90ba9a3de69eed2</truc:Passkey>
     	<truc:ResellerId>TESTRESELLER</truc:ResellerId>      	
	</truc:deleteVehicle>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

__Example SOAP 1.1 response__
```	
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <m:deleteVehicleResponse xmlns:m="http://www.trucks.nl/">
         <m:deleteVehicleResult>
            <m:code>202</m:code>
            <m:vehicleid>123456</m:vehicleid>
            <m:description>Vehicle succesfully deleted</m:description>
         </m:deleteVehicleResult>
      </m:deleteVehicleResponse>
   </soap:Body>
</soap:Envelope>
```
!!! tip ""
    The service also supports SOAP 1.2 and JSON calls for more information click [here](http://www.trucks.nl/StockAPI/TrucksStock.wso?op=deleteVehicle)


###getPublicDealerInfo

This function can be used to check the validation and dealer status before sending in vehicle mutation requests. 

| Parameter | Description |
| --------- | ----------- | 
| DealerId  | Unique dealer identifier |
| ResellerId | Unique reseller identifier | 
| Passkey   | Hashed key |

__Example SOAP 1.1 request__ 

```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:truc="http://www.trucks.nl/">
   <soapenv:Header/>
   <soapenv:Body>
      <truc:getPublicDealerInfo>
         <truc:DealerId>TESTDEALER</truc:DealerId>
         <truc:Passkey>123456768692ea9ac90ba9a3de69eed2</truc:Passkey>
         <truc:ResellerId>TESTRESELLER</truc:ResellerId>
      </truc:getPublicDealerInfo>
   </soapenv:Body>
</soapenv:Envelope>
```

__Example SOAP 1.1 response__ 

```
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <m:getPublicDealerInfoResponse xmlns:m="http://www.trucks.nl/">
         <m:getPublicDealerInfoResult>
            <m:DealerId>TESTDEALER</m:DealerId>
            <m:CurVehicles>1</m:CurVehicles>
            <m:MaxVehicles>150</m:MaxVehicles>
            <m:PicProVehicle>15</m:PicProVehicle>
            <m:Active>Y</m:Active>
            <m:StockOnTrucks>Y</m:StockOnTrucks>
         </m:getPublicDealerInfoResult>
      </m:getPublicDealerInfoResponse>
   </soap:Body>
</soap:Envelope>
```

!!! tip ""
    The service also supports SOAP 1.2 and JSON calls for more information click [here](http://www.trucks.nl/StockAPI/TrucksStock.wso?op=getPublicDealerInfo)

!!! danger "HTTP 500"
    If a dealer doesn't have access to this API or isn't found a HTTP 500 error will be returned. 


###getDealerStock

| Parameter | Description |
| --------- | ----------- | 
| DealerId  | Unique dealer identifier |
| ResellerId | Unique reseller identifier | 
| Passkey   | Hashed key |

This function returns current stock of a dealer. For each vehicle values that are not default (as indicated by the [getKeyDescriptions](#getkeydescriptions) function response) are returned in order to synchronise stocks on Trucks.nl and resller side. 

__Exampe SOAP 1.1 request__

```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:truc="http://www.trucks.nl/">
   <soapenv:Header/>
   <soapenv:Body>
      <truc:getDealerStock>
         <truc:DealerId>TESTDEALER</truc:DealerId>
         <truc:Passkey>123456768692ea9ac90ba9a3de69eed2</truc:Passkey>
         <truc:ResellerId>TESTRESELLER</truc:ResellerId>
      </truc:getDealerStock>
   </soapenv:Body>
</soapenv:Envelope>
```

__Example SOAP 1.1 response__

```
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <m:getDealerStockResponse xmlns:m="http://www.trucks.nl/">
         <m:getDealerStockResult>
            <m:DealerId>TESTDEALER</m:DealerId>
            <m:max_vehicles>150</m:max_vehicles>
            <m:cur_vehicles>1</m:cur_vehicles>
            <m:lastupdated>2017-09-19 02:03:00.603</m:lastupdated>
            <m:type_dealer>Trucks</m:type_dealer>
            <m:vehicles>
               <m:tVehicleInfo>
                  <m:VehicleId>4256280</m:VehicleId>
                  <m:DealerId>TESTDEALER</m:DealerId>
                  <m:pictures>
                     <m:string>http://www.trucks.nl/truckpics/size5/4256280_1.jpg</m:string>
                     <m:string>http://www.trucks.nl/truckpics/size5/4256280_2.jpg</m:string>
                     <m:string>http://www.trucks.nl/truckpics/size5/4256280_3.jpg</m:string>
                     <m:string>http://www.trucks.nl/truckpics/size5/4256280_4.jpg</m:string>
                     <m:string>http://www.trucks.nl/truckpics/size5/4256280_5.jpg</m:string>
                     <m:string>http://www.trucks.nl/truckpics/size5/4256280_6.jpg</m:string>
                  </m:pictures>
                  <m:values>
                     <m:tValue>
                        <m:key>VEHICLE_TYPE_ID</m:key>
                        <m:value>TRCK</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>MAKE_ID</m:key>
                        <m:value>SCANIA</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>TYPE</m:key>
                        <m:value>P124GB8X4B</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>YEAR</m:key>
                        <m:value>2002</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>CONSTRUCTION_ID</m:key>
                        <m:value>DIV</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>PICTURE_COUNT</m:key>
                        <m:value>6</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>PUBLISH</m:key>
                        <m:value>Y</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>DATE_CREATED</m:key>
                        <m:value>2017-09-19 02:03:00.587</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>DATE_UPDATED</m:key>
                        <m:value>2017-09-19 02:03:00.587</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>EXTRA_EURO</m:key>
                        <m:value>3</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>EXTRA_AIRSUSP</m:key>
                        <m:value>Y</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>EXTRA_DISCBRAKE</m:key>
                        <m:value>Y</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>EXTRA_NEWUSED</m:key>
                        <m:value>1</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>EXTRA_RADIOCD</m:key>
                        <m:value>Y</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>MAX_WEIGHT</m:key>
                        <m:value>32000</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>MILAGE</m:key>
                        <m:value>460000</m:value>
                     </m:tValue>
                     <m:tValue>
                        <m:key>AXL_CONFIG</m:key>
                        <m:value>8x4</m:value>
                     </m:tValue>
                  </m:values>
               </m:tVehicleInfo>      
               <m:tVehicleInfo>      
               		...
               </m:tVehicleInfo>               
            </m:vehicles>
         </m:getDealerStockResult>
      </m:getDealerStockResponse>
   </soap:Body>
</soap:Envelope>
```

!!! tip ""
    The service also supports SOAP 1.2 and JSON calls for more information click [here](http://www.trucks.nl/StockAPI/TrucksStock.wso?op=getDealerStock)

!!! warning ""
	The value-pair for the value 'DESCRIPTION' is ommitted in this function


###getKeyDescriptions

| Parameter | Description |
| --------- | ----------- | 
| Language  | Language code (only English is supported at this time) |


All values that can be used to describe a vehicle on Trucks.nl will be returned by this function, including all information on those values and their constrains. 

!!! tip "Vehicletypes, makes and constructions"
	Vehicles on Trucks.nl are categorised by vehicltypes, each category has a number of subcategories called constructions, which are defined by construction_id's. For each category there is a list of make's available, these are defined by make_id's. 

	More information about these categories can be found on the [categories](/Categories/) page. 

__Example ValueDescription response in SOAP 1.1__

```
<m:ValueDescription>
   <m:key>VEHICLE_TYPE_ID</m:key>
   <m:datatype>char</m:datatype>
   <m:max_length>4</m:max_length>
   <m:description>Category of the vehicle (is validated, can't be empty)</m:description>
   <m:possible_values>Info in other webservice => OptionsService.wso</m:possible_values>
   <m:default_value/>
   <m:nullable>false</m:nullable>
</m:ValueDescription>
<m:ValueDescription>
   <m:key>DLR_VEHICLE_ID</m:key>
   <m:datatype>char</m:datatype>
   <m:max_length>50</m:max_length>
   <m:description>Stock nr used by dealer</m:description>
   <m:possible_values/>
   <m:default_value/>
   <m:nullable>true</m:nullable>
</m:ValueDescription>
<m:ValueDescription>
   <m:key>MAKE_ID</m:key>
   <m:datatype>char</m:datatype>
   <m:max_length>15</m:max_length>
   <m:description>Make_id for vehicle</m:description>
   <m:possible_values>Info in other webservice => OptionsService.wso</m:possible_values>
   <m:default_value>DIV.</m:default_value>
   <m:nullable>true</m:nullable>
</m:ValueDescription>
<m:ValueDescription>
   <m:key>TYPE</m:key>
   <m:datatype>char</m:datatype>
   <m:max_length>75</m:max_length>
   <m:description>Type description for vehicle</m:description>
   <m:possible_values/>
   <m:default_value/>
   <m:nullable>true</m:nullable>
</m:ValueDescription>

```

!!! tip ""
    The service also supports SOAP 1.2 and JSON calls for more information click [here](http://www.trucks.nl/StockAPI/TrucksStock.wso?op=getKeyDescriptions)